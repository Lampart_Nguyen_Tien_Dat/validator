package com.lampartvn.validator;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.lampartvn.validator.constraints.AllowCharacter;
import com.lampartvn.validator.constraints.CheckDateFormat;
import com.lampartvn.validator.constraints.CheckDateTimeFormat;
import com.lampartvn.validator.constraints.CheckOneByteString;
import com.lampartvn.validator.constraints.CheckTimeFormat;
import com.lampartvn.validator.constraints.DatetimeCompare;
import com.lampartvn.validator.constraints.IsKatakana;
import com.lampartvn.validator.constraints.IsKatakanaOneByte;
import com.lampartvn.validator.constraints.MaxByteString;
import com.lampartvn.validator.constraints.ValidEmail;
import com.lampartvn.validator.constraints.ValidMobileEmail;
import com.lampartvn.validator.constraints.ValidPhoneNumber;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ValidatorApplicationTests {

	private static ValidatorFactory validatorFactory;
	private static Validator validator;
	private static final String INPUT_DATA_PATH = "/TestDataInput/";

	AllowCharacter allowCharacter;

	@BeforeAll
	public static void createValidator() {
		validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@AfterAll
	public static void close() {
		validatorFactory.close();

	}

	// test for constrains @AllowCharacter
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "AllowCharacterTestDataInput.csv", numLinesToSkip = 1)
	public void testAllowCharacter(String name, String input, int expect) {

		// set input to model
		DummyModel allowCharacterModel = new DummyModel() {
			@AllowCharacter(allow = "katakana + hiragana + whitespace-2byte")
			String test = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(allowCharacterModel);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @CheckDateFormat
//	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "CheckDateFormatTestDataInput.csv", numLinesToSkip = 1)
	public void testCheckDateFormat(String name, String input, int expect) {
		// set input to model
		DummyModel checkDateFormat = new DummyModel() {
			@CheckDateFormat()
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(checkDateFormat);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @CheckDateTimeFormat
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "CheckDateTimeFormatTestDataInput.csv", numLinesToSkip = 1)
	public void testCheckDateTimeFormat(String name, String input, int expect) {

		// set input to model
		DummyModel checkDateTimeFormat = new DummyModel() {
			@CheckDateTimeFormat
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(checkDateTimeFormat);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @CheckOneByteString
	@Disabled
	@ParameterizedTest(name = "{index}. {0} ")
	@CsvFileSource(resources = INPUT_DATA_PATH + "CheckOneByteStringTestDataInput.csv", numLinesToSkip = 1)
	public void testCheckOneByteString(String name, String input, int expect) {

		// set input to model
		DummyModel checkOneByteString = new DummyModel() {
			@CheckOneByteString
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(checkOneByteString);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @CheckTimeFormat
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "CheckTimeFormatTestDataInput.csv", numLinesToSkip = 1)
	public void testCheckTimeFormat(String name, String input, int expect) {

		// set input to model
		DummyModel checkTimeFormat = new DummyModel() {
			@CheckTimeFormat
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(checkTimeFormat);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @DatetimeCompare
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "DatetimeCompareTestDataInput.csv", numLinesToSkip = 1)
	public void testDatetimeCompare(String name, String input, int expect) {

		// set input to model
		DummyModel datetimeCompare = new DummyModel() {
			@DatetimeCompare(condition = "before 2019-01-15")
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(datetimeCompare);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @IsKatakana
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "IsKatakanaTestDataInput.csv", numLinesToSkip = 1)
	public void testIsKatakana(String name, String input, int expect) {

		// set input to model
		DummyModel isKatakana = new DummyModel() {

			@IsKatakana
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(isKatakana);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @IsKatakanaOneByte
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "IsKatakanaOneByteTestDataInput.csv", numLinesToSkip = 1)
	public void testIsKatakanaOneByte(String name, String input, int expect) {

		// set input to model
		DummyModel isKatakanaOneByte = new DummyModel() {

			@IsKatakanaOneByte
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(isKatakanaOneByte);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @MaxByteString
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "MaxByteStringTestDataInput.csv", numLinesToSkip = 1)
	public void testMaxByteString(String name, String input, int expect) {

		// set input to model
		DummyModel maxByteString = new DummyModel() {

			@MaxByteString(maxByte = 20)
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(maxByteString);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @ValidEmail
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "ValidEmailTestDataInput.csv", numLinesToSkip = 1)
	public void testValidEmail(String name, String input, int expect) {

		// set input to model
		DummyModel validEmail = new DummyModel() {

			@ValidEmail
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(validEmail);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @ValidMobileEmail
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "ValidMobileEmailTestDataInput.csv", numLinesToSkip = 1)
	public void testValidMobileEmail(String name, String input, int expect) {

		// set input to model
		DummyModel validMobileEmail = new DummyModel() {

			@ValidMobileEmail
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(validMobileEmail);

		// check violations
		assertEquals(expect, violations.size());

	}

	// test for constrains @ValidPhoneNumber
	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = INPUT_DATA_PATH + "ValidPhoneNumberTestDataInput.csv", numLinesToSkip = 1)
	public void testValidPhoneNumber(String name, String input, int expect) {

		// set input to model
		DummyModel validPhoneNumber = new DummyModel() {

			@ValidPhoneNumber
			String param = input;
		};

		// validate
		Set<ConstraintViolation<DummyModel>> violations = validator.validate(validPhoneNumber);

		// check violations
		assertEquals(expect, violations.size());

	}

}
