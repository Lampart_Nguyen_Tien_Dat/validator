package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampartvn.validator.constraintvalidators.CheckTimeFormatValidator;

/**
 * Check if input time is valid.<br>
 * Default format is "HH:mm:ss". <br>
 * 
 * @author tien_dat
 *
 */
@Documented
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckTimeFormatValidator.class)
public @interface CheckTimeFormat {

	String message() default "{com.lampartvn.validator.constraints.CheckTimeFormat.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	// Japanese time format is default
	String pattern() default "HH:mm:ss";

}
