package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampartvn.validator.constraintvalidators.IsKatakanaOneByteValidator;

/**
 * The string must contains Katakana 1-byte characters.
 *
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = IsKatakanaOneByteValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsKatakanaOneByte {
	String message() default "{com.lampartvn.validator.constraints.IsKatakanaOneByte.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
