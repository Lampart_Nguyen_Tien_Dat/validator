package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

/**
 * The string has to be a well-formed email address.
 *
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = {})
@Pattern(regexp = "^([a-z0-9\\+_\\-\\.\\/]+)([a-z0-9\\+_\\-\\.\\/]+)@([a-z0-9\\-]+\\.)+[a-z]{2,6}$", flags = {
		Flag.CASE_INSENSITIVE, Flag.COMMENTS }, message = "{com.lampartvn.validator.constraints.ValidEmail.message}")
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEmail {
	String message() default "{com.lampartvn.validator.constraints.ValidEmail.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
