package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampartvn.validator.constraintvalidators.CheckDateFormatValidator;

/**
 * Check if input date is valid.<br>
 * Default format is "yyyy-MM-dd". <br>
 * 
 * @author tien_dat
 *
 */
@Documented
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckDateFormatValidator.class)
public @interface CheckDateFormat {

    String message() default "{com.lampartvn.validator.constraints.CheckDateFormat.message}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    // Japanese date format is default
    String pattern() default "yyyy-MM-dd";

}
