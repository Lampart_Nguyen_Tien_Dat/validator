package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampartvn.validator.constraintvalidators.CheckDateTimeFormatValidator;

/**
 * Check if input datetime is valid.<br>
 * Default format is "yyyy-MM-dd HH:mm:ss". <br>
 * 
 * @author tien_dat
 *
 */
@Documented
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckDateTimeFormatValidator.class)
public @interface CheckDateTimeFormat {

    String message() default "{com.lampartvn.validator.constraints.CheckDateTimeFormat.message}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    // Japanese date time format is default
    String pattern() default "yyyy-MM-dd HH:mm:ss";

}
