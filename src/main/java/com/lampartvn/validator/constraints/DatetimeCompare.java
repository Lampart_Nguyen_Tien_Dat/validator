package com.lampartvn.validator.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampartvn.validator.constraintvalidators.DatetimeCompareValidator;

/**
 * Compare input datetime. <br>
 * Default format is "yyyy-MM-dd". <br>
 * Allow compare operators is "after", "before", ">", ">=", "=", "<", "<=". <br>
 * Beside default format datetime string , it allow these strings: "today", "now".
 *
 * <pre>
 * Example: "before now" <br>
 *     Check if input date is before now
 * </pre>
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = DatetimeCompareValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DatetimeCompare {
	String message() default "{com.lampartvn.validator.constraints.DatetimeCompare.message}";

	String condition() default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
