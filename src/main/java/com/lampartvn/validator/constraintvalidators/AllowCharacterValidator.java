package com.lampartvn.validator.constraintvalidators;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampartvn.validator.constraints.AllowCharacter;

public class AllowCharacterValidator implements ConstraintValidator<AllowCharacter, String> {

	private String allow;

	private static final Map<String, String> replace = new HashMap<String, String>();
	{
		replace.put("alpha", "a-zA-Z");
		replace.put("numeric", "0-9");
		replace.put("katakana", "\\x{30A0}-\\x{30FF}");
		replace.put("hiragana", "\\x{3040}-\\x{309F}");
		replace.put("kanji", "\\x{4E00}-\\x{9FBF}");
		replace.put("furigana", "\\x{FF5F}-\\x{FF9F}");
		replace.put("whitespace", " ");
		replace.put("verticalline", "|");
		replace.put("j-symbol-punctuation", "\\x{3000}-\\x{303F}");
		replace.put("j-miscellaneous", "\\x{31F0}-\\x{31FF}\\x{3220}-\\x{3243}\\x{3280}-\\x{337F}");
		replace.put("j-alphanumeric", "\\x{FF01}-\\x{FF5E}");
		replace.put("j-numeric", "\\x{FF10}-\\x{FF19}");
		replace.put("kanji-radicals", "\\x{2E80}-\\x{2FD5}");
		replace.put("kanji-rare", "\\x{3400}-\\x{4BDF}");
		replace.put("whitespace-2byte", "　");
	}

	@Override
	public void initialize(AllowCharacter constraintAnnotation) {
		allow = constraintAnnotation.allow();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		String[] allowSplit = allow.split(" \\+ ");

		StringBuilder allowString = new StringBuilder();
		for (Map.Entry<String, String> entry : replace.entrySet()) {

			if (Arrays.asList(allowSplit).contains(entry.getKey())) {
				allowString.append(entry.getValue());
			}

		}

		// check allow

		Pattern regex = Pattern.compile("^[" + allowString.toString() + "]+$");

		Matcher matcher = regex.matcher(value);

		return matcher.matches();
	}

}
