package com.lampartvn.validator.constraintvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampartvn.validator.constraints.MaxByteString;

public class MaxByteStringValidator implements ConstraintValidator<MaxByteString, String> {

	private int maxByte;

	@Override
	public void initialize(MaxByteString constraintAnnotation) {
		maxByte = constraintAnnotation.maxByte();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraintContext) {

		// get value byte lenght
		int stringByteLength = value.getBytes().length;

		// compare
		return (maxByte >= stringByteLength);
	}
}
