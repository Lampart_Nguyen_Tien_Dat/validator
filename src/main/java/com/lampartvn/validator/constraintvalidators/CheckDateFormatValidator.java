package com.lampartvn.validator.constraintvalidators;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampartvn.validator.constraints.CheckDateFormat;

public class CheckDateFormatValidator implements ConstraintValidator<CheckDateFormat, String> {

	private String pattern;

	@Override
	public void initialize(CheckDateFormat constraintAnnotation) {
		this.pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {

		if (object == null) {
			return true;
		}

		// set pattern
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		// use this function to check valid date
		dateFormat.setLenient(false);

		try {

			Date date = dateFormat.parse(object.trim());

			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
