package com.lampartvn.validator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidatorApplication {

	public static void main(String[] args) {

		// Set default locale
//		Locale.setDefault(Locale.JAPAN);

		SpringApplication.run(ValidatorApplication.class, args);
	}

}
